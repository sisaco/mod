package net.mcreator.the_ultimate_combat_update;

import net.minecraft.util.math.Vec3d;
import net.minecraft.potion.Effects;
import net.minecraft.potion.EffectInstance;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import java.util.Collection;

@Elementsthe_ultimate_combat_update.ModElement.Tag
public class MCreatorImpedimentaBulletHitsLivingEntity extends Elementsthe_ultimate_combat_update.ModElement {
	public MCreatorImpedimentaBulletHitsLivingEntity(Elementsthe_ultimate_combat_update instance) {
		super(instance, 21);
	}

	public static void executeProcedure(java.util.HashMap<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure MCreatorImpedimentaBulletHitsLivingEntity!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof LivingEntity)
			((LivingEntity) entity).addPotionEffect(new EffectInstance(Effects.SLOWNESS, (int) 200, (int) 255, (false), (false)));
		while ((new Object() {
			boolean check() {
				if (entity instanceof LivingEntity) {
					Collection<EffectInstance> effects = ((LivingEntity) entity).getActivePotionEffects();
					for (EffectInstance effect : effects) {
						if (effect.getPotion() == Effects.SLOWNESS)
							return true;
					}
				}
				return false;
			}
		}.check())) {
			entity.setMotionMultiplier(null, new Vec3d(0.25D, (double) 0.05F, 0.25D));
		}
	}
}
