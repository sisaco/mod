package net.mcreator.the_ultimate_combat_update;

import net.minecraftforge.items.ItemHandlerHelper;

import net.minecraft.world.World;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;

@Elementsthe_ultimate_combat_update.ModElement.Tag
public class MCreatorBullet2ItemInInventoryTick extends Elementsthe_ultimate_combat_update.ModElement {
	public MCreatorBullet2ItemInInventoryTick(Elementsthe_ultimate_combat_update instance) {
		super(instance, 11);
	}

	public static void executeProcedure(java.util.HashMap<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure MCreatorBullet2ItemInInventoryTick!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure MCreatorBullet2ItemInInventoryTick!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		World world = (World) dependencies.get("world");
		if (((the_ultimate_combat_updateVariables.MapVariables.get(world).gi_noe) == 1)) {
			if (entity instanceof PlayerEntity)
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) entity), new ItemStack(MCreatorBullet.block, (int) (1)));
		}
	}
}
